import dotenv from 'dotenv';

dotenv.config();

const env = {
  app: {
    port: process.env.APP_PORT,
    domain: process.env.DOMAIN,
    socketPort: process.env.SOCKET_PORT,
    secret: process.env.SECRET_KEY
  },
  db: {
    database: process.env.DB_NAME,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: process.env.DB_DIALECT,
    logging: false
  },
  imgur: {
    imgurId: process.env.IMGUR_ID,
    imgurSecret: process.env.IMGUR_SECRET
  },
  mailgun: {
    DOMAIN: "sandbox978bc1a3114647e882e7c1da8225e876.mailgun.org",
    apiKey: "09d31ff2033e53a97b6feb44c6500921-1b6eb03d-20c1780c"
  }
};

export default env;
