export default (err, req, res, next) => {
  if (res.headersSent) { // http://expressjs.com/en/guide/error-handling.html
    next(err);
  } else {
    const { status = 500, message = '', errors = [] } = err;
    const detailedMessage = errors.length > 0
      ? `${message}: ${errors.map(({ message }) => message).join(', ')}`
      : message;
    res.status(status).send({ status, message: detailedMessage });
  }
};
