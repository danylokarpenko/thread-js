import { Router } from 'express';
import * as commentService from '../services/commentService';

const router = Router();

router
  .get('/', (req, res, next) => commentService.getComments(req.query)
    .then(comments => res.send(comments))
    .catch(next))
  .get('/react', (req, res, next) => commentService.getReactions(req.query)
    .then(reactions => res.send(reactions))
    .catch(next))
  .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.post && (reaction.comment.userId !== req.user.id)) {
        // notify a user if someone (not himself) liked his comment
        req.io.to(reaction.comment.userId).emit('like', 'Your comment was liked!');
      }
      return res.send(reaction);
    })
    .catch(next))
  .put('/:id', (req, res, next) => commentService.update(req.params.id, req.body)
    .then(comment => {
      req.io.emit('comment_update', comment); // notify all users that the comment was updated
      return res.send(comment);
    })
    .catch(next))
  .delete('/:id', (req, res, next) => commentService.softDelete(req.params.id)
    .then(response => {
      req.io.emit('comment_delete'); // notify all users that the post was deleted
      return res.send({ response });
    })
    .catch(next));


export default router;
