import { Router, request } from 'express';
import * as authService from '../services/authService';
import * as userService from '../services/userService';
import * as resetPasswordService from '../services/resetPasswordService';
import authenticationMiddleware from '../middlewares/authenticationMiddleware';
import registrationMiddleware from '../middlewares/registrationMiddleware';
import jwtMiddleware from '../middlewares/jwtMiddleware';
const router = Router();

// user added to the request (req.user) in a strategy, see passport config
router
  .post('/login', authenticationMiddleware, (req, res, next) => authService.login(req.user)
    .then(data => res.send(data))
    .catch(next))
  .post('/register', registrationMiddleware, (req, res, next) => authService.register(req.user)
    .then(data => res.send(data))
    .catch(next))
  .get('/user', jwtMiddleware, (req, res, next) => userService.getUserById(req.user.id)
    .then(data => res.send(data))
    .catch(next))
  .put('/user', (req, res, next) => userService.update(req.user.id, req.body)
    .then(user => {
      // req.io.emit('user_update', user); // notify current user that the profile was updated
      return res.send(user);
    })
    .catch(next))
  .post('/forgot-password', async (req, res, next) => resetPasswordService.create(req.body.email)
    .then(body => res.send(body))
    .catch(next))
  .get('/reset-password', async (req, res, next) => resetPasswordService.getToken(req.query)
    .then(() => res.redirect(200, `/api/auth/reset-password/${req.query.token}/${req.query.email}`))
    .catch(next))
  .post('/reset-password', async (req, res, next) => resetPasswordService.resetPassword(req.body)
    .then()
    .catch(next));
export default router;
