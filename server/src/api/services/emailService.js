const mailgun = require("mailgun-js");

import env from '../../env';

const mg = mailgun({ apiKey: env.mailgun.apiKey, domain: env.mailgun.DOMAIN });

export const sendResetEmail = (email, token) => {
    const resetLink = `http://${env.app.domain}/api/auth/reset-password/${token}/${email}`;

    const data = {
        from: "Mailgun Sandbox <postmaster@sandbox978bc1a3114647e882e7c1da8225e876.mailgun.org>",
        to: 'karpenkodanylo@gmail.com',
        subject: "Reset password",
        text: `To reset your password, please click the link below.\n\n${resetLink}`
    };

    return new Promise((resolve, reject) => {
        mg.messages().send(data, function (error, body) {
            if (error) reject(error);
            resolve(body);
        });
    });
}