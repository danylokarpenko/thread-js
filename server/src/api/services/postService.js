import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';

export const getPosts = async filter => {
  const posts = await postRepository.getPosts(filter)
  return posts;
};

export const getPostById = async id => {
  const post = await postRepository.getPostById(id)
  return post;
};

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const softDelete = (id) => postRepository.softDeleteById(id);

export const update = (id, post) => postRepository.updateById(id, { ...post });

export const setReaction = async (userId, { postId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);
  
  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};

export const getReactions = async filter => await postReactionRepository.getPostReactions(filter);