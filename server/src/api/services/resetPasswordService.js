import resetTokenRepository from '../../data/repositories/resetTokenRepository';
import { createResetToken } from '../../helpers/tokenHelper';
import { sendResetEmail } from './emailService';
import * as userService from './userService';
import { encrypt } from '../../helpers/cryptoHelper';

export const getToken = async filter => {
    return await resetTokenRepository.getToken(filter);
}

export const getByEmail = async email => await resetTokenRepository.getByEmail(email);

export const create = async email => {
    const user = await userService.getUserByEmail(email);
    if (!user) {
        throw Error('No user with this email!')
    }

    // Expire any tokens that were previously set for this user.
    // That prevents old tokens from being used.
    await resetTokenRepository.deleteByEmail(email);

    const expireDate = new Date();
    expireDate.setDate(expireDate.getDate() + 1 / 24);
    const token = createResetToken();

    const createdToken = await resetTokenRepository.create({
        email,
        expiration: expireDate,
        token
    });
    
    if (!createdToken) return;
    sendResetEmail(email, token);
    return createdToken;
};

export const resetPassword = async request => {    
    const { email, token, password } = request;
    
    const resetToken = await resetTokenRepository.getToken({ email, token });
    
    if (resetToken == null) {
        throw Error('Token not found. Please try the reset password process again.');
    }

    const user = await userService.getUserByEmail(email);

    if (!user) {
        throw Error('No user with this email!')
    }

    const updatedUser = await userService.update(user.id, { password: await encrypt(password) });
    await resetTokenRepository.deleteById(resetToken.id); 
    
    return updatedUser;
}
