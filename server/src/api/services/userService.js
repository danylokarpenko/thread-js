import userRepository from '../../data/repositories/userRepository';

export const getUserById = async userId => {
  const { id, username, email, imageId, image, status } = await userRepository.getUserById(userId);
  return { id, username, email, imageId, image, status };
};

export const getUserByEmail = async userEmail => {
  const { id, username, email, imageId, image, status } = await userRepository.getByEmail(userEmail);
  return { id, username, email, imageId, image, status };
};

export const update = async (userId, data) => await userRepository.updateById(userId, data);