import jwt from 'jsonwebtoken';
import crypto from 'crypto';
import { secret, expiresIn } from '../config/jwtConfig';

export const createToken = data => jwt.sign(data, secret, { expiresIn });

export const createResetToken = () => crypto.randomBytes(32).toString('hex');