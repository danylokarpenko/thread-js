export default [
  '/auth/login',
  '/auth/register',
  '/auth/reset-password',
  '/auth/forgot-password'
];
