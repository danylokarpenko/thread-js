export default models => {
  const {
    User,
    Post,
    PostReaction,
    CommentReaction,
    Comment,
    Image,
    ResetToken
  } = models;

  Image.hasOne(User);
  Image.hasOne(Post);

  ResetToken.hasOne(User);

  User.hasMany(Post);
  User.hasMany(Comment);
  User.hasMany(PostReaction);
  User.hasMany(CommentReaction);
  User.belongsTo(Image);
  User.belongsTo(ResetToken);

  Post.belongsTo(Image);
  Post.belongsTo(User);
  Post.hasMany(PostReaction);
  Post.hasMany(Comment);

  Comment.belongsTo(User);
  Comment.belongsTo(Post);
  Comment.hasMany(CommentReaction);

  PostReaction.belongsTo(Post);
  PostReaction.belongsTo(User);

  CommentReaction.belongsTo(User);
  CommentReaction.belongsTo(Comment);
};
