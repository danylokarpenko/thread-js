'use strict';
module.exports = (sequelize, DataTypes) => {
  const resetToken = sequelize.define('resetToken', {
    email: DataTypes.STRING,
    token: DataTypes.STRING,
    expiration: DataTypes.DATE
  }, {});
  return resetToken;
};