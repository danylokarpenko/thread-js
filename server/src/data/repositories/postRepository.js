import { Op } from 'sequelize';
import sequelize from '../db/connection';
import { PostModel, CommentModel, UserModel, ImageModel, PostReactionModel } from '../models/index';
import BaseRepository from './baseRepository';

const likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class PostRepository extends BaseRepository {
  softDeleteById(id) {
    return this.deleteById(id);
  }
  // вытягивает только посты с реакцией тру от юзера с ИД, но и реакции вытягивает только соответсвенные
  // надо составить запрос который 
  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId,
      notUserId,
      likedByUser
    } = filter;

    let where = {};

    if (userId) {
      Object.assign(where, { userId });
    }
    if (notUserId) {
      Object.assign(where, {
        userId: {
          [Op.not]: notUserId
        }
      })
    }
    if (likedByUser) {
      where = sequelize.where(sequelize.literal(`CASE WHEN "postReactions"."isLike" = true AND "postReactions"."userId"::text = '${likedByUser}' THEN 1 ELSE 0 END`), 1)
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId"
                        AND "comment"."deletedAt" is NULL)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount'],
        ]
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: PostReactionModel,
        attributes: [],
        duplicating: false,
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId"
                        AND "comment"."deletedAt" is NULL)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: CommentModel,
        include: {
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: PostReactionModel,
        attributes: []
      }]
    });
  }

  async getLikedPosts(filter) {

    return this.model.findAll({
      where,
      include: {
        model: PostReactionModel,
        attributes: [],
        duplicating: false,
        where: reactionFilter
      },
      group: [
        'post.id',
        'postReactions.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  };
}

export default new PostRepository(PostModel);

