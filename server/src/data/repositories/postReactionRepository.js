import { PostReactionModel, PostModel, UserModel, ImageModel } from '../models/index';
import BaseRepository from './baseRepository';

class PostReactionRepository extends BaseRepository {
  getPostReaction(userId, postId) {
    return this.model.findOne({
      group: [
        'postReaction.id',
        'post.id'
      ],
      where: { userId, postId },
      include: [{
        model: PostModel,
        attributes: ['id', 'userId']
      }]
    });
  }
  
  getPostReactions(filter) {
    const {
      postId,
      isLike
    } = filter;

    const where = {};

    if (postId) {
      Object.assign(where, { postId })
    }
    if (isLike) {
      Object.assign(where, { isLike })
    }

    return this.model.findAll({
      where,
      include: [{
        model: UserModel,
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        },
        attributes: ['id', 'username']
      }]
    })
  }
}

export default new PostReactionRepository(PostReactionModel);
