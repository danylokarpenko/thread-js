import { Op } from 'sequelize';
import sequelize from '../db/connection';
import { ResetTokenModel } from '../models/index';
import BaseRepository from './baseRepository';

class ResetTokenRepository extends BaseRepository {
  getToken(filter) {
    const {
      token,
      email
    } = filter;

    const where = {
      expiration: { [Op.gt]: new Date().getDate() }
    };
    
    if (token) {
      Object.assign(where, { token });
    }
    if (email) {
      Object.assign(where, { email });
    }

    return this.model.findOne({ where });
  }
  
  add(resetPassword) {
    return this.create(resetPassword);
  }

  getByEmail(email) {
    return this.model.findOne({ where: { email } });
  }

  deleteByEmail(email) {
    return this.model.destroy({
      where: { email }
    });
  }
}

export default new ResetTokenRepository(ResetTokenModel);
