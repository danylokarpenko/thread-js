import { CommentModel, CommentReactionModel, UserModel, ImageModel } from '../models/index';
import BaseRepository from './baseRepository';

class CommentReactionRepository extends BaseRepository {
  getCommentReaction(userId, commentId) {
    return this.model.findOne({
      group: [
        'commentReaction.id',
        'comment.id'
      ],
      where: { userId, commentId },
      include: [{
        model: CommentModel,
        attributes: ['id', 'userId']
      }]
    });
  }

  getCommentReactions(filter) {
    const {
      commentId,
      isLike
    } = filter;

    const where = {};

    if (commentId) {
      Object.assign(where, { commentId })
    }
    if (isLike) {
      Object.assign(where, { isLike })
    }

    return this.model.findAll({
      where,
      include: [{
        model: UserModel,
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        },
        attributes: ['id', 'username']
      }]
    })
  }
}

export default new CommentReactionRepository(CommentReactionModel);
