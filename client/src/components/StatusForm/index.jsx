import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Input } from 'semantic-ui-react';

const StatusUpdateForm = ({
  userStatus,
  updateProfile,
  setIsUpdating
}) => {
  const [status, setStatus] = useState(userStatus);

  const handleStatusChange = ev => {
    const stat = ev.target.value;
    setStatus(stat);
  };

  const handleUpdateStatus = async () => {
    await updateProfile({ status });
    setIsUpdating(false);
  };

  return (
    <Form onSubmit={handleUpdateStatus}>
      <Input
        size="small"
        icon="bullhorn"
        iconPosition="left"
        placeholder="Set status"
        type="text"
        value={status}
        onChange={handleStatusChange}
      />
      <Button
        type="submit"
        color="green"
        size="small"
        basic
        icon
        content="Save"
      />
      <Button
        color="red"
        size="small"
        basic
        icon
        content="Cancel"
        onClick={() => setIsUpdating(false)}
      />
    </Form>
  );
};

StatusUpdateForm.propTypes = {
  setIsUpdating: PropTypes.func.isRequired,
  updateProfile: PropTypes.func.isRequired,
  userStatus: PropTypes.string
};

StatusUpdateForm.defaultProps = {
  userStatus: ''
};

export default StatusUpdateForm;
