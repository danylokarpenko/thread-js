import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Image, Button } from 'semantic-ui-react';

const UpdatePost = ({
  post,
  updatePost
}) => {
  const {
    id,
    image
  } = post;

  const [body, setBody] = useState(post.body || '');

  const handleUpdatePost = async () => {
    if (!body) {
      return;
    }
    await updatePost(id, { body });
  };

  return (
    <Form reply onSubmit={handleUpdatePost}>
      {image && <Image src={image.link} wrapped bordered />}
      <Form.TextArea
        value={body}
        name="body"
        placeholder="What's new?"
        onChange={ev => setBody(ev.target.value)}
      />
      <Button type="submit" content="Update" primary />
    </Form>
  );
};

UpdatePost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  updatePost: PropTypes.func.isRequired
};

export default UpdatePost;
