import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button } from 'semantic-ui-react';

const UpdateComment = ({
  comment,
  updateComment,
  handle: handleOnUpdate
}) => {
  const { id } = comment;
  const [body, setBody] = useState(comment.body || '');

  const handleUpdateComment = async () => {
    if (!body) {
      return;
    }
    await updateComment(id, { body });
    handleOnUpdate();
  };

  return (
    <Form reply onSubmit={handleUpdateComment}>
      <Form.TextArea
        value={body}
        placeholder="Type a comment..."
        onChange={ev => setBody(ev.target.value)}
      />
      <Button type="submit" content="Update comment" labelPosition="left" icon="edit" primary />
    </Form>
  );
};

UpdateComment.propTypes = {
  updateComment: PropTypes.func.isRequired,
  handle: PropTypes.func.isRequired,
  comment: PropTypes.objectOf(PropTypes.any).isRequired
};

export default UpdateComment;
