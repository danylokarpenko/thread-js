import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
// import validator from 'validator';
import { Form, Button, Segment } from 'semantic-ui-react';

const ResetPasswordForm = ({ match, resetPassword }) => {
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isLoading, setLoading] = useState(false);
  const [isPasswordValid, setPasswordValid] = useState(true);
  const [isConfirmPasswordValid, setConfirmPasswordValid] = useState(true);
  const [redirect, setRedirect] = useState(false);

  const passwordChanged = value => {
    setPassword(value);
    setPasswordValid(true);
  };

  const confirmPasswordChanged = value => {
    setConfirmPassword(value);
    setConfirmPasswordValid(true);
  };

  const isEquel = (pass1, pass2) => pass1 === pass2;

  const reset = async () => {
    const isValid = isConfirmPasswordValid && isPasswordValid && isEquel(password, confirmPassword);
    if (!isValid || isLoading) {
      return;
    }
    setLoading(true);
    try {
      await resetPassword({ password, token: match.params.token, email: match.params.email });
      setRedirect(true);
    } catch {
      setLoading(false);
    }
  };

  return (
    <div>
      {redirect ? (
        <Redirect to="/login" />
      ) : (
        <Form name="resetPasswordForm" size="large" onSubmit={reset}>
          <Segment>
            <Form.Input
              fluid
              icon="lock"
              iconPosition="left"
              placeholder="Password"
              type="password"
              onChange={ev => passwordChanged(ev.target.value)}
              error={!isPasswordValid}
              onBlur={() => setPasswordValid(Boolean(password))}
            />
            <Form.Input
              fluid
              icon="lock"
              iconPosition="left"
              placeholder="Confirm password"
              type="password"
              onChange={ev => confirmPasswordChanged(ev.target.value)}
              error={!isConfirmPasswordValid}
              onBlur={() => setConfirmPasswordValid(Boolean(password))}
            />
            <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
              Set new password
            </Button>
          </Segment>
        </Form>
      )}
    </div>
  );
};

ResetPasswordForm.propTypes = {
  resetPassword: PropTypes.func.isRequired,
  match: PropTypes.objectOf(PropTypes.any).isRequired
};

export default ResetPasswordForm;
