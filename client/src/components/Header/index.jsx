import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Header as HeaderUI, Image, Grid, Icon, Button } from 'semantic-ui-react';
import StatusForm from 'src/components/StatusForm';
import styles from './styles.module.scss';

const Header = ({ user, logout, updateProfile }) => {
  const [isStatusUpdate, setIsStatusUpdate] = useState(false);

  return (
    <div className={styles.headerWrp}>
      <Grid centered container columns="2">
        <Grid.Column>
          {user && (
            <div className={styles.header}>
              <NavLink exact to="/">
                <HeaderUI>
                  <Image circular src={getUserImgLink(user.image)} />
                  {' '}
                </HeaderUI>
              </NavLink>
              <div className={styles.usermeta}>
                <NavLink exact to="/">
                  <HeaderUI>
                    {user.username}
                  </HeaderUI>
                </NavLink>
                {isStatusUpdate ? (
                  <StatusForm
                    userStatus={user.status}
                    updateProfile={updateProfile}
                    setIsUpdating={setIsStatusUpdate}
                  />
                )
                  : (
                    <div>
                      {user.status}
                      <Button
                        size="tiny"
                        basic
                        icon
                        as="a"
                        className={`${styles.menuBtn} ${styles.logoutBtn}`}
                        onClick={() => setIsStatusUpdate(true)}
                      >
                        <Icon name="pencil" />
                      </Button>
                    </div>
                  )}
              </div>
            </div>
          )}
        </Grid.Column>
        <Grid.Column textAlign="right">
          <NavLink exact activeClassName="active" to="/profile" className={styles.menuBtn}>
            <Icon name="user circle" size="large" />
          </NavLink>
          <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={logout}>
            <Icon name="log out" size="large" />
          </Button>
        </Grid.Column>
      </Grid>
    </div>
  );
};

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  updateProfile: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Header;
