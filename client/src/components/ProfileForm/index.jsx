import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { getUserImgLink, getCroppedImage } from 'src/helpers/imageHelper';
import { checkUsername, formErrorMessage } from 'src/helpers/validations';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';

import {
  Image,
  Input,
  Button,
  Form,
  Icon,
  Label
} from 'semantic-ui-react';

import styles from './styles.module.scss';

const ProfileForm = ({ user, updateProfile, uploadImage }) => {
  const [username, setUsername] = useState(user.username);
  const [usernameErrors, setUsernameErrors] = useState(false);
  const [status, setStatus] = useState(user.status);
  const [isUploading, setIsUploading] = useState(false);
  const [src, setSrc] = useState(undefined);
  const [imageRef, setImageRef] = useState(undefined);
  const [croppedImage, setCroppedImage] = useState(undefined);
  const [crop, setCrop] = useState({
    width: 100,
    height: 100,
    aspect: 1 / 1
  });

  const handleUsernameChange = ev => {
    const userName = ev.target.value;
    setUsernameErrors(checkUsername(userName));
    setUsername(userName);
  };

  const handleStatusChange = ev => {
    const userStatus = ev.target.value;
    setStatus(userStatus);
  };

  const handleUpdateProfile = async () => {
    if (usernameErrors.length > 0) {
      return;
    }
    try {
      await updateProfile({ username, status });
    } catch (e) {
      setUsernameErrors([...usernameErrors, e]);
    }
  };

  const onSelectFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const reader = new FileReader();
      reader.addEventListener('load', () => setSrc(reader.result));
      reader.readAsDataURL(target.files[0]);
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  const handleCropComplete = async c => {
    if (imageRef && c.width && c.height) {
      const croppedIm = await getCroppedImage(imageRef, c, 'newAvatar.jpeg');
      setCroppedImage(croppedIm);
    }
  };

  const handleUploadAvatar = async () => {
    if (!croppedImage) return;
    const { id: imageId } = await uploadImage(croppedImage);
    await updateProfile({ imageId });
    setSrc(undefined);
    setImageRef(undefined);
    setCroppedImage(undefined);
  };

  const onImageLoaded = im => {
    const props = {
      crossOrigin: 'Anonymous'
    };
    Object.assign(im, props);
    setImageRef(im);
  };

  return (
    <div>
      <Form onSubmit={handleUploadAvatar}>
        {src ? (
          <div>
            <ReactCrop
              maxWidth={200}
              maxHeight={200}
              style={{ width: '300px', height: 'auto' }}
              src={src}
              crop={crop}
              onImageLoaded={onImageLoaded}
              onComplete={handleCropComplete}
              onChange={newCrop => setCrop(newCrop)}
            />
            <br />
            <div>
              <Button type="sumbit" color="green" icon circular>
                <Icon name="check" />
              </Button>
              <Button onClick={() => setSrc(undefined)} color="red" icon circular>
                <Icon name="times" />
              </Button>
            </div>
          </div>
        )
          : (
            <div>
              <Image
                centered
                src={getUserImgLink(user.image)}
                size="medium"
                circular
              />
              <br />
              <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
                <Icon name="file image" />
                Browse avatar
                <input name="image" type="file" onChange={onSelectFile} hidden />
              </Button>
            </div>
          )}
      </Form>
      <Form reply onSubmit={handleUpdateProfile}>
        <br />
        <Input
          className={usernameErrors.length > 0 ? styles.error : null}
          icon="user"
          iconPosition="left"
          placeholder="Username"
          type="text"
          value={username}
          onChange={handleUsernameChange}
        />
        <br />
        {usernameErrors.length > 0 && (
          <Label basic color="red" pointing>
            {formErrorMessage(usernameErrors)}
          </Label>
        )}
        <br />
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={user.email}
        />
        <br />
        <br />
        <Input
          icon="bullhorn"
          iconPosition="left"
          placeholder="Status"
          type="text"
          value={status}
          onChange={handleStatusChange}
        />
        <br />
        <br />
        <Button primary type="submit" content="Save changes" />
      </Form>
    </div>
  );
};

ProfileForm.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  updateProfile: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

export default ProfileForm;
