import React from 'react';
import PropTypes from 'prop-types';
import { List, Image } from 'semantic-ui-react';
import { getUserImgLink } from 'src/helpers/imageHelper';

const Reactions = ({
  reactions
}) => (
  <List divided verticalAlign="middle">
    {reactions.map(({ user }) => (
      <List.Item key={user.id}>
        <Image avatar src={getUserImgLink(user.image)} />
        <List.Content>
          <List.Header as="a">{user.username}</List.Header>
        </List.Content>
      </List.Item>
    ))}
  </List>
);

Reactions.propTypes = {
  reactions: PropTypes.arrayOf(PropTypes.any).isRequired
};

export default Reactions;
