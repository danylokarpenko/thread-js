import React, { useState } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { Form, Button, Segment } from 'semantic-ui-react';

const ForgotPasswordForm = ({ change }) => {
  const [email, setEmail] = useState('');
  const [isLoading, setLoading] = useState(false);
  const [isEmailValid, setEmailValid] = useState(true);

  const emailChanged = value => {
    setEmail(value);
    setEmailValid(true);
  };

  const changePassword = async () => {
    const isValid = isEmailValid;
    if (!isValid || isLoading) {
      return;
    }
    setLoading(true);
    try {
      await change({ email });
    } catch {
      setLoading(false);
    }
  };

  return (
    <Form name="ForgotPasswordForm" size="large" onSubmit={changePassword}>
      <Segment>
        <Form.Input
          fluid
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          error={!isEmailValid}
          onChange={ev => emailChanged(ev.target.value)}
          onBlur={() => setEmailValid(validator.isEmail(email))}
        />
        <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
          Change password
        </Button>
      </Segment>
    </Form>
  );
};

ForgotPasswordForm.propTypes = {
  change: PropTypes.func.isRequired
};

export default ForgotPasswordForm;
