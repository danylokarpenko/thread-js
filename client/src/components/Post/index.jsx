import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Dropdown } from 'semantic-ui-react';
import moment from 'moment';

import styles from './styles.module.scss';

const Post = ({
  userId,
  post,
  likePost,
  dislikePost,
  deletePost,
  toggleExpandedPost,
  sharePost,
  toggleEdittingPost,
  toggleShowReactions
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = moment(createdAt).fromNow();

  const handleDeletePost = async () => {
    if (!id) return;
    await deletePost(id);
  };

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped bordered />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
          <Dropdown>
            <Dropdown.Menu>
              <Dropdown.Item
                icon="thumbs up outline"
                text="Show likes"
                onClick={() => toggleShowReactions({ postId: id, isLike: true })}
              />
              <Dropdown.Item
                icon="thumbs down outline"
                text="Show dislikes"
                onClick={() => toggleShowReactions({ postId: id, isLike: false })}
              />
              {userId === post.userId
              && <Dropdown.Item icon="edit" text="Edit" onClick={() => toggleEdittingPost(id)} />}
              {userId === post.userId
              && <Dropdown.Item icon="trash" text="Delete" onClick={handleDeletePost} />}
            </Dropdown.Menu>
          </Dropdown>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleEdittingPost: PropTypes.func.isRequired,
  userId: PropTypes.string,
  sharePost: PropTypes.func.isRequired,
  toggleShowReactions: PropTypes.func.isRequired
};

Post.defaultProps = {
  userId: undefined
};

export default Post;
