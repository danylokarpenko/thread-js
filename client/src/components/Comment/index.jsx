import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon, Dropdown } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import UpdateComment from 'src/components/UpdateComment';

import styles from './styles.module.scss';

const Comment = ({
  comment,
  userId,
  likeComment,
  dislikeComment,
  updateComment,
  deleteComment,
  toggleShowReactions
}) => {
  const { id, body, createdAt, user, likeCount, dislikeCount } = comment;
  const [isUpdate, setIsUpdate] = useState(false);

  const handleDeleteComment = async commentId => {
    if (!commentId) return;
    await deleteComment(commentId);
  };

  const handleOnUpdate = () => {
    setIsUpdate(!isUpdate);
  };

  return (
    <CommentUI>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      {!isUpdate ? (
        <CommentUI.Content>
          <CommentUI.Author as="a">
            {user.username}
          </CommentUI.Author>
          <CommentUI.Metadata>
            {moment(createdAt).fromNow()}
            <Dropdown>
              <Dropdown.Menu>
                <Dropdown.Item
                  icon="thumbs up outline"
                  text="Show likes"
                  onClick={() => toggleShowReactions({ commentId: id, isLike: true })}
                />
                <Dropdown.Item
                  icon="thumbs down outline"
                  text="Show dislikes"
                  onClick={() => toggleShowReactions({ commentId: id, isLike: false })}
                />
                {userId === user.id
                && <Dropdown.Item icon="edit" text="Edit" onClick={handleOnUpdate} />}
                {userId === user.id
                && <Dropdown.Item icon="trash" text="Delete" onClick={() => handleDeleteComment(id)} />}
              </Dropdown.Menu>
            </Dropdown>
          </CommentUI.Metadata>
          <CommentUI.Text>
            <div className={styles.status}>
              <Icon name="bullhorn" />
              <span>
                {user.status}
              </span>
            </div>
          </CommentUI.Text>
          <CommentUI.Text>
            {body}
          </CommentUI.Text>
          <CommentUI.Actions>
            <CommentUI.Action onClick={() => likeComment(id)}>
              <Icon name="thumbs up" />
              {likeCount}
            </CommentUI.Action>
            <CommentUI.Action onClick={() => dislikeComment(id)}>
              <Icon name="thumbs down" />
              {dislikeCount}
            </CommentUI.Action>
          </CommentUI.Actions>
        </CommentUI.Content>
      )
        : (
          <CommentUI.Content>
            <UpdateComment comment={comment} updateComment={updateComment} handle={handleOnUpdate} />
          </CommentUI.Content>
        )}
    </CommentUI>
  );
};

Comment.propTypes = {
  userId: PropTypes.string,
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  toggleShowReactions: PropTypes.func.isRequired
};

Comment.defaultProps = {
  userId: undefined
};

export default Comment;
