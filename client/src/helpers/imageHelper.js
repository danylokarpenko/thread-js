export const getUserImgLink = image => (image
  ? image.link
  : 'https://forwardsummit.ca/wp-content/uploads/2019/01/avatar-default.png');

export const getCroppedImage = (im, c) => {
  const canvas = document.createElement('canvas');
  const scaleX = im.naturalWidth / im.width;
  const scaleY = im.naturalHeight / im.height;
  canvas.width = c.width;
  canvas.height = c.height;
  const ctx = canvas.getContext('2d');

  ctx.drawImage(
    im,
    c.x * scaleX,
    c.y * scaleY,
    c.width * scaleX,
    c.height * scaleY,
    0,
    0,
    c.width,
    c.height
  );

  return new Promise((resolve, reject) => {
    canvas.toBlob(blob => {
      if (!blob) {
        reject(new Error('Canvas is empty'));
        return;
      }
      resolve(blob);
    }, 'image/jpeg');
  });
};
