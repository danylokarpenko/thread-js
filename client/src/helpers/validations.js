import validator from 'validator';

export const checkUsername = username => {
  const errors = [];

  if (!validator.isLength(username, { min: 2 })) {
    errors.push({ message: 'Minimum 2!' });
  }
  if (!validator.isLength(username, { max: 20 })) {
    errors.push({ message: 'Maximum 20!' });
  }

  return errors;
};

export const formErrorMessage = errors => errors.map(({ message }) => message).join(' ');
