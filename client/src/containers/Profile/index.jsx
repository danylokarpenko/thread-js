import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  Grid
} from 'semantic-ui-react';
import ProfileForm from 'src/components/ProfileForm';
import * as imageService from 'src/services/imageService';

import { updateProfile } from './actions';

const Profile = ({ user, updateProfile: updateUser }) => {
  const uploadImage = file => imageService.uploadImage(file);
  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <ProfileForm user={user} updateProfile={updateUser} uploadImage={uploadImage} />
      </Grid.Column>
    </Grid>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  updateProfile: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {}
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

const actions = {
  updateProfile
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
