import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import {
  likePost,
  deletePost,
  dislikePost,
  toggleExpandedPost,
  toggleEdittingPost,
  addComment,
  likeComment,
  dislikeComment,
  deleteComment,
  updateComment,
  toggleCommentReactions,
  togglePostReactions
} from 'src/containers/Thread/actions';

import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

const ExpandedPost = ({
  userId,
  post,
  sharePost,
  likePost: like,
  deletePost: removePost,
  dislikePost: dislike,
  toggleExpandedPost: toggleExpantion,
  toggleEdittingPost: toggleEditting,
  addComment: add,
  likeComment: likeComm,
  dislikeComment: dislikeComm,
  deleteComment: removeComment,
  updateComment: updateComm,
  toggleCommentReactions: toggleCommentReact,
  togglePostReactions: togglePostReact
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggleExpantion()}>
    {post
      ? (
        <Modal.Content>
          <Post
            userId={userId}
            post={post}
            likePost={like}
            deletePost={removePost}
            dislikePost={dislike}
            toggleExpandedPost={toggleExpantion}
            toggleEdittingPost={toggleEditting}
            sharePost={sharePost}
            toggleShowReactions={togglePostReact}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <Header as="h3" dividing>
              Comments
            </Header>
            {post.comments && post.comments
              .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
              .map(comment => (
                <Comment
                  userId={userId}
                  key={comment.id}
                  comment={comment}
                  likeComment={likeComm}
                  dislikeComment={dislikeComm}
                  deleteComment={removeComment}
                  updateComment={updateComm}
                  toggleShowReactions={toggleCommentReact}
                />
              ))}
            <AddComment postId={post.id} addComment={add} />
          </CommentUI.Group>
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

ExpandedPost.defaultProps = {
  userId: undefined
};

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleEdittingPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired,
  toggleCommentReactions: PropTypes.func.isRequired,
  togglePostReactions: PropTypes.func.isRequired,
  userId: PropTypes.string
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost,
  updatingComment: rootState.posts.updatingComment,
  userId: rootState.profile.user.id
});

const actions = {
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment,
  likeComment,
  dislikeComment,
  deletePost,
  toggleEdittingPost,
  deleteComment,
  updateComment,
  toggleCommentReactions,
  togglePostReactions
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
