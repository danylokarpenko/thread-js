import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal } from 'semantic-ui-react';
import {
  togglePostReactions
} from 'src/containers/Thread/actions';

import Reactions from 'src/components/Reactions';
import Spinner from 'src/components/Spinner';

const ShowReactions = ({
  reactions,
  togglePostReactions: toggle
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle(undefined)}>
    {reactions
      ? (
        <Modal.Content>
          <Reactions reactions={reactions} />
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

ShowReactions.defaultProps = {
  reactions: undefined
};

ShowReactions.propTypes = {
  reactions: PropTypes.arrayOf(PropTypes.any),
  togglePostReactions: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  reactions: rootState.posts.showReactions
});

const actions = {
  togglePostReactions
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShowReactions);
