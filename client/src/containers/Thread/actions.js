import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST,
  SET_EDIT_POST,
  SET_SHOW_REACT
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

const setEdittingPostAction = post => ({
  type: SET_EDIT_POST,
  post
});

const setShowReactions = reactions => ({
  type: SET_SHOW_REACT,
  reactions
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));

  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  const postComments = await commentService.getAllComments({ postId: id });

  dispatch(addPostAction({ ...newPost, comments: postComments }));
};

export const updatePost = (id, postData) => async (dispatch, getRootState) => {
  const { id: postId } = await postService.updatePost(id, postData);
  const updatedPost = await postService.getPost(postId);
  const comments = await commentService.getAllComments({ postId });

  const { posts: { posts } } = getRootState();
  const updatedPosts = posts.map(post => (post.id !== postId ? post : { ...updatedPost, comments }));

  dispatch(setPostsAction(updatedPosts));
};

export const updateComment = (id, commentData) => async (dispatch, getRootState) => {
  const { id: commentId } = await commentService.updateComment(id, commentData);
  const updatedComment = await commentService.getComment(commentId);

  const mapComments = post => ({
    ...post,
    comments: (post.comments || []).map(comm => (comm.id !== commentId ? comm : updatedComment))
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updatedPosts = posts.map(post => (post.id !== updatedComment.postId ? post : mapComments(post)));

  dispatch(setPostsAction(updatedPosts));

  if (expandedPost && expandedPost.id === updatedComment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  const comments = await commentService.getAllComments({ postId });

  dispatch(setExpandedPostAction(post ? { ...post, comments } : post));
};

export const toggleEdittingPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;

  dispatch(setEdittingPostAction(post));
};

export const likeComment = commentId => async (dispatch, getRootState) => {
  await commentService.likeComment(commentId);
  const updatedComment = await commentService.getComment(commentId);

  const mapLikes = post => ({
    ...post,
    comments: (post.comments || []).map(comm => (comm.id !== updatedComment.id ? comm : updatedComment))
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updatedPosts = posts.map(post => (post.id !== updatedComment.postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updatedPosts));

  if (expandedPost && expandedPost.id === updatedComment.postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
  await commentService.dislikeComment(commentId);
  const updatedComment = await commentService.getComment(commentId);

  const mapLikes = post => ({
    ...post,
    comments: (post.comments || []).map(comm => (comm.id !== updatedComment.id ? comm : updatedComment))
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updatedPosts = posts.map(post => (post.id !== updatedComment.postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updatedPosts));

  if (expandedPost && expandedPost.id === updatedComment.postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const deletePost = postId => async (dispatch, getRootState) => {
  if (!postId) return;
  await postService.deletePostById(postId);

  const { posts: { posts } } = getRootState();
  const filteredPosts = posts.filter(post => post.id !== postId);

  dispatch(setPostsAction(filteredPosts));
};

export const deleteComment = commentId => async (dispatch, getRootState) => {
  if (!commentId) return;
  const { postId } = await commentService.getComment(commentId);
  await commentService.deleteCommentById(commentId);

  const filterComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) - 1,
    comments: (post.comments || []).filter(comment => comment.id !== commentId)
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updatedPosts = posts.map(post => (post.id !== postId ? post : filterComments(post)));

  dispatch(setPostsAction(updatedPosts));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(filterComments(expandedPost)));
  }
};

export const likePost = postId => async (dispatch, getRootState) => {
  const { id, updatedAt, createdAt } = await postService.likePost(postId);
  const diff = id ? 1 : -1;
  const isUpdated = updatedAt === createdAt;

  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diff,
    dislikeCount: isUpdated ? Number(post.dislikeCount) : Number(post.dislikeCount) - diff
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const { id, updatedAt, createdAt } = await postService.dislikePost(postId);
  const diff = id ? 1 : -1;
  const isUpdated = updatedAt === createdAt;

  const mapDislikes = post => ({
    ...post,
    dislikeCount: Number(post.dislikeCount) + diff,
    likeCount: isUpdated ? Number(post.likeCount) : Number(post.likeCount) - diff
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment]
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const togglePostReactions = filter => async dispatch => {
  const reactions = filter ? await postService.getReactions(filter) : undefined;

  dispatch(setShowReactions(reactions));
};

export const toggleCommentReactions = filter => async dispatch => {
  const reactions = filter ? await commentService.getReactions(filter) : undefined;

  dispatch(setShowReactions(reactions));
};
