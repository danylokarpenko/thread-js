/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import EdittingPost from 'src/containers/EdittingPost';
import ShowReactions from 'src/containers/ShowReactions';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
  loadPosts,
  deletePost,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  toggleEdittingPost,
  addPost,
  togglePostReactions
} from './actions';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10
};

const Thread = ({
  userId,
  loadPosts: load,
  deletePost: removePost,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  edittingPost,
  hasMorePosts,
  addPost: createPost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggleExpantion,
  toggleEdittingPost: toggleEditting,
  togglePostReactions: togglePostReact,
  showReactions
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [hideOwnPosts, setHideOwnPosts] = useState(false);
  const [showLikedPosts, setShowLikedPosts] = useState(false);

  const toggleShowOwnPosts = () => {
    if (hideOwnPosts) {
      setHideOwnPosts(false);
      postsFilter.notUserId = undefined;
    }
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleHideOwnOwnPosts = () => {
    if (showOwnPosts) {
      setShowOwnPosts(false);
      postsFilter.userId = undefined;
    }
    setHideOwnPosts(!hideOwnPosts);
    postsFilter.notUserId = hideOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowLikedPosts = () => {
    setShowLikedPosts(!showLikedPosts);
    postsFilter.likedByUser = showLikedPosts ? undefined : userId;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show other's people posts"
          checked={hideOwnPosts}
          onChange={toggleHideOwnOwnPosts}
        />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show posts that I liked"
          checked={showLikedPosts}
          onChange={toggleShowLikedPosts}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            userId={userId}
            post={post}
            deletePost={removePost}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggleExpantion}
            toggleEdittingPost={toggleEditting}
            sharePost={sharePost}
            key={post.id}
            toggleShowReactions={togglePostReact}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} />}
      {edittingPost && <EdittingPost />}
      {sharedPostId && <SharedPostLink postId={sharedPostId} close={() => setSharedPostId(undefined)} />}
      {showReactions && <ShowReactions />}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  edittingPost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleEdittingPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  togglePostReactions: PropTypes.func.isRequired,
  showReactions: PropTypes.arrayOf(PropTypes.object)
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  edittingPost: undefined,
  userId: undefined,
  showReactions: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  edittingPost: rootState.posts.edittingPost,
  showReactions: rootState.posts.showReactions,
  userId: rootState.profile.user.id
});

const actions = {
  loadPosts,
  deletePost,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  toggleEdittingPost,
  addPost,
  togglePostReactions
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
