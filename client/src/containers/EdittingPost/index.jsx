import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'semantic-ui-react';
import Spinner from 'src/components/Spinner';
import UpdatePost from 'src/components/UpdatePost';
import { updatePost, toggleEdittingPost } from 'src/containers/Thread/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

const EdittingPost = ({
  post,
  updatePost: update,
  toggleEdittingPost: toggle
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {post
      ? (
        <Modal.Content>
          <UpdatePost post={post} updatePost={update} />
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

EdittingPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  updatePost: PropTypes.func.isRequired,
  toggleEdittingPost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.edittingPost
});

const actions = { updatePost, toggleEdittingPost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EdittingPost);
